### 배포 링크
https://lunchbox-97110.web.app/

### 프로젝트 설명
학교 다닐때 샐랩 (Sal Lab)이라는 샐러드를 배달해주는 음식점이 있었다. 여기서 샐러드를 주문하기 위해서는 전화를 하거나 네이버 카페에 접속해야 했는데, 샐랩 자체의 웹사이트가 있으면 좋겠다라는 생각을 했었다. 그래서 샐랩을 위한 웹사이트를 React를 공부하는 김에 한번 만들어 보자는 생각에 프로젝트를 시작하였다. 예전에 Lunch Box라는 회사가 있었는데 이 회사의 웹사이트의 디자인을 토대로 만들었다. 그래서 프로젝트 이름을 Lunch Box라고 하였다.

웹사이트는 5개의 페이지로 구성되어있다.
WE ARE SALLAB - 샐랩의 소개를 확인할 수 있다.
MENU - 샐랩의 메뉴를 볼 수 있으며 주문 기능은 구현하지 않았다.
SALLABERS - 샐랩의 공지 게시판으로 글 내용을 확인하고 댓글을 작성할 수 있다.
MY SALLAB - 샐랩에서 주문한 내용을 확인할 수 있다. 유져별 주문 기능은 구현하지 않았다.
LOGIN - 샐랩의 로그인 화면으로 실질적인 기능은 구현하지 않았다.

### 개발 블로그 링크

https://jongbeom-dev.tistory.com/2?category=835620

## Available Scripts

이 프로젝트에서는 다음의 스크립트를 이용한다.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### `firebase deploy`

App is deployed to firebase server.
Url of the deployed app is [https://lunchbox-97110.web.app](https://lunchbox-97110.web.app).